package Fahrkartenautomat;

import java.io.IOException;
import java.util.Scanner;

class Fahrkartenautomat {

	public static double fahrkartenbestellungErfassen() {
		int anzahlTickets;

		Scanner tastatur = new Scanner(System.in);

		int ticketArt = 0;
//        tastatur.close();
		while (ticketArt == 0) {
			System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus: ");
			System.out.println("[1] Einzelfahrschein Regeltarif AB [3,00 EUR]");
			System.out.println("[2] Tageskarte Regeltarif AB [8,60 EUR]");
			System.out.println("[3] Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR]");

			ticketArt = tastatur.nextInt();
			switch (ticketArt) {
			case 1:
				ticketArt = 300;
				break;
			case 2:
				ticketArt = 860;
				break;
			case 3:
				ticketArt = 2350;
				break;
			default:
				System.out.println(">> ung�ltige Eingabe! <<\n");
				ticketArt = 0;
			}
		}

		System.out.print("Anzahl der Tickets (max. 10): ");
		anzahlTickets = tastatur.nextInt();

		while (anzahlTickets > 10 || anzahlTickets < 1) {
			System.out.println("\n>> ung�ltige Eingabe! <<\n");
			System.out.println("W�hlen Sie bitte eine Anzahl zwischen 1 bis 10 Tickets aus.\n");
			anzahlTickets = tastatur.nextInt();
		}

		return ticketArt * anzahlTickets;
	}

	public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
		double eingeworfenemuenze;
		double eingezahlterGesamtbetrag = 0.0;
		Scanner tastatur = new Scanner(System.in);
//        tastatur.close();

		while (eingezahlterGesamtbetrag < (zuZahlenderBetrag / 100)) {
			System.out.format("\nNoch zu zahlen: %4.2f � %n", ((zuZahlenderBetrag / 100) - eingezahlterGesamtbetrag));
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfenemuenze = tastatur.nextDouble();

			if (eingeworfenemuenze == 0.05 || eingeworfenemuenze == 0.1 || eingeworfenemuenze == 0.2
					|| eingeworfenemuenze == 0.5 || eingeworfenemuenze == 1 || eingeworfenemuenze == 2) {
				eingezahlterGesamtbetrag += eingeworfenemuenze;
			} else {
				System.out.println("\nM�nze nicht erkannt!");
			}

		}
		return eingezahlterGesamtbetrag - (zuZahlenderBetrag / 100);
	}

	public static void fahrkartenAusgeben() {
		System.out.println("\nFahrschein wird ausgegeben");
		for (int i = 0; i < 8; i++) {
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}

	public static void rueckgeldAusgeben(double rueckgabebetrag) {

		if (rueckgabebetrag > 0.0) {
			System.out.format("Der R�ckgabebetrag in H�he von %4.2f Euro %n", rueckgabebetrag);
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while (rueckgabebetrag >= 2.0) {// 2 EURO-muenzen
				System.out.println("2 EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 2.0);
			}
			while (rueckgabebetrag >= 1.0) {// 1 EURO-muenzen
				System.out.println("1 EURO");
				rueckgabebetrag = runden(rueckgabebetrag -= 1.0);
			}
			while (rueckgabebetrag >= 0.5) // 50 CENT-muenzen
			{
				System.out.println("50 CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.5);
			}
			while (rueckgabebetrag >= 0.2) // 20 CENT-muenzen
			{
				System.out.println("20 CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.2);
			}
			while (rueckgabebetrag >= 0.1) // 10 CENT-Müzen
			{
				System.out.println("10 CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.1);
			}
			while (rueckgabebetrag >= 0.05)// 5 CENT-muenzen
			{
				System.out.println("5 CENT");
				rueckgabebetrag = runden(rueckgabebetrag -= 0.05);
			}
		}
	}

	public static double runden(double zahl) {
		zahl = Math.round(zahl * 100) / 100.00;
		return zahl;

	}

	public static void main(String[] args) {
		double zuZahlenderBetrag;
		double rueckgabebetrag;

		do {
			zuZahlenderBetrag = fahrkartenbestellungErfassen();
			rueckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag);
			fahrkartenAusgeben();
			rueckgeldAusgeben(rueckgabebetrag);
			System.out.println("\nVergessen Sie nicht, den Fahrschein\n" + "vor Fahrtantritt entwerten zu lassen!\n"
					+ "Wir w�nschen Ihnen eine gute Fahrt.\n\n\n");
			System.out.println(">> Enter um Fortzufahren. <<");

			try {
				System.in.read();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} while (true);
	}

}