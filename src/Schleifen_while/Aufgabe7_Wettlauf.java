package Schleifen_while;

public class Aufgabe7_Wettlauf {

	public static void main(String[] args) {
		double metera = 9.5;
		double meterb = 243;
		int timer = -1;
		String kopfzeile1= "Zeit in [s]";
		String kopfzeile2= "Sprinter A [m]";
		String kopfzeile3= "Sprinter B [m]";
		System.out.printf ("%8.20s" + "%18.20s" + "%18.20s" + "\n" ,kopfzeile1, kopfzeile2, kopfzeile3);
		
		while (metera <= 1000 && meterb <= 1000) {
			timer++;
			metera += 9.5;
			meterb += 7;
			System.out.printf ("%6d" + "s" + "%18.2f" + " m" + "%16.2f" + " m" + "\n" ,timer, metera, meterb);
		}

	}

}
