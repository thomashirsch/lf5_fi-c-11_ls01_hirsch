package Schleifen_while;

import java.util.Scanner;

public class Aufgabe8_Matrix {

	public static void main(String[] args) {
		System.out.println("Bitte geben Sie eine Zahl zwischen 2 und 9 ein:");
		Scanner myScanner = new Scanner(System.in);
		int eingabe = myScanner.nextInt();
		myScanner.close();
		int zaehler = 0;

		while (zaehler < 100) {
			int zahl = zaehler;
			int quersumme = 0;
			boolean pruefer = true;
			// berechnung Quersumme
			while (zahl >= 1) {
				quersumme = quersumme + zahl % 10;
				zahl = zahl / 10;
			}
			// abfrage Quersumme?, durch Eingabe teilbar?, Eingabe als Zahl enthalten?
			if (quersumme == eingabe || zaehler % eingabe == 0 || (zaehler - eingabe) % 10 == 0
					|| (zaehler / 10 - eingabe) % 10 == 0) {
				pruefer = false;
			}
			// Formatierungshilfe
			if (zaehler % 10 == 0) {
				System.out.print("\n");
			}
			if (zaehler < 10 || pruefer == false) {
				System.out.print(" ");
			}
			// Ausgabe
			if (pruefer == true) {
				System.out.print(zaehler + " ");
			} else {
				System.out.print("* ");
			}
			zaehler++;
		}

	}

}
