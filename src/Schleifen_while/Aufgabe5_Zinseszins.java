package Schleifen_while;

import java.util.Scanner;

public class Aufgabe5_Zinseszins {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Laufzeit (in Jahren) des Sparvertrags:");
		int dauer = myScanner.nextInt();
		System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen:");
		double kapital = myScanner.nextDouble();
		System.out.println("Zinssatz:");
		double zinssatz = myScanner.nextDouble();
		myScanner.close();
		System.out.printf("Eingezahltes Kapital: %.2f �\n", kapital);
		berechnung(dauer, kapital, zinssatz);

	}

	public static void berechnung(int dauer, double guthaben, double zinssatz) {
		while (dauer >= 1) {
			dauer--;
			guthaben += (guthaben * (zinssatz / 100));
		}
		System.out.printf("Ausgezahltes Kapital: %.2f �", guthaben);

	}

}
