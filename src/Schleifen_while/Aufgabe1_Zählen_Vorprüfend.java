package Schleifen_while;

import java.util.Scanner;

public class Aufgabe1_Z�hlen_Vorpr�fend {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine ganze Zahl ein.");
		int eingabe = myScanner.nextInt();
		int hilfszahl = 1;
		myScanner.close();
		
		System.out.println("\n Aufsteigende Reihe");
		while(eingabe >= hilfszahl) {
			System.out.print(hilfszahl + " ");
			hilfszahl++;
		}

		System.out.println("\n\n Absteigende Reihe");
		while(eingabe > 0) {
			System.out.print(eingabe + " ");
			eingabe--;
		}
		
	}

}
