package Schleifen_while;

import java.util.Scanner;

public class Aufgabe4_Temperaturumrechnung {

	public static void main(String[] args) {
		System.out.println("Umrechnung von Grad Celsius nach Fahrenheit.");
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie den Startwert ein:");
		double start = myScanner.nextDouble();
		System.out.println("Bitte geben Sie den Endwert ein:");
		double ende = myScanner.nextDouble();
		System.out.println("Bitte geben Sie die Schrittweite ein:");
		double schritt = myScanner.nextDouble();
		myScanner.close();
		berechnung(start, ende, schritt);
	}
		
		public static void berechnung(double start, double ende, double schritt) {
			double fahrenheit = (start * 1.8) + 32;
			
			while (start <= ende) {
				System.out.printf ("%8.2f" + "�C" + "%15.2f" + "�F" + "\n" ,start, fahrenheit);
				start = start + schritt;
				fahrenheit = (start * 1.8) + 32;

			}
	}

		
	

}
