package Schleifen_while;

import java.util.Scanner;

public class Aufgabe6_Million {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		char wdh = 'y';

		while (wdh != 'n') {
			if (wdh != 'y') {
				System.out.println("ung�ltige Eingabe");
				wdh = myScanner.next().charAt(0);
			} 
			
			else {

				System.out.println("Wie viel Kapital (in Euro) m�chten Sie anlegen: ");
				double kapital = myScanner.nextDouble();
				System.out.println("Zinssatz in %: ");
				double zinssatz = myScanner.nextDouble();
				int counter = 0;
				while (kapital < 1000000) {
					counter++;
					kapital += (kapital * (zinssatz / 100));

					
				}

				System.out.println("\nBis zur Million sind es gerade einmal " + counter + " Jahre.");
				System.out.println("Ihr Verm�gen betr�gt dann " + Math.round(100*kapital)/100.0 + " �.");
				System.out.println("\nSoll eine weitere Berechnung durchgef�hrt werden? (y/n)");
				wdh = myScanner.next().charAt(0);
			}

		}
	}
}