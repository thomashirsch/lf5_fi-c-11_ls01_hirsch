package Schleifen_while;

import java.util.Scanner;

public class Aufgabe3_Quersumme_Nachpr�fend {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie die Zahl ein, von der die Quersumme ermittelt werden soll.");
		int eingabe = myScanner.nextInt();
		int quersumme = 0;
		myScanner.close();
				
		do {
			quersumme = quersumme + eingabe %10;
			eingabe = eingabe / 10;
			
		}
		while (eingabe >= 1); 

		System.out.println(quersumme);
	}

}
