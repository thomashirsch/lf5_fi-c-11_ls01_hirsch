package Schleifen_while;

import java.util.Scanner;

public class Aufgabe2_Fakultaet {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm ermittelt die Fakult�t.");
		System.out.println("Bitte geben Sie eine ganze Zahl ein, die nicht gr��er als 20 ist.");
		int eingabe = myScanner.nextInt();
		long ergebnis = 1;
		
		myScanner.close();
		
		if (eingabe == 0) {
			System.out.println("1");
		}
		else {
		if (eingabe >= 1 && eingabe <= 20) { 
			do {
				ergebnis = ergebnis * eingabe;
				eingabe--;
			}
			while(eingabe > 0);

			System.out.println(ergebnis);
		}
		else {
			System.out.println("Ung�ltige Eingabe!");
		}
		}

	}
	
	
}
