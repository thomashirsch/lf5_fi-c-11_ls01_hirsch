package Fallunterscheidung_switch_case;

import java.util.Scanner;

public class Aufgabe1_Noten {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		char nochmal = 'n';
		do
		{
		System.out.println( "Bitte geben Sie die Note als Zahl ein.");
	
		int note = myScanner.nextInt();	
			
		switch (note) {
		case 1:
			System.out.println("sehr gut");
			break;
		case 2:
			System.out.println("gut");
			break;
		case 3:
			System.out.println("befriedigend");
			break;
		case 4:
			System.out.println("ausreichend");
			break;
		case 5:
			System.out.println("mangelhaft");
			break;
		case 6:
			System.out.println("ungenügend");
			break;
		default:
			System.out.println("ungültige Eingabe!");	
			}

		System.out.println("\nSoll eine weitere Note eingegeben werden? (y/n)");
		nochmal = myScanner.next().charAt(0);
		}
		while(nochmal != 'n');

		myScanner.close();

	}
}
