package Schleifen_for;

public class Aufgabe3_Modulo {

	public static void main(String[] args) {
			
		sieben();
		System.out.println("\n");
		fuenfvier();
		
		}
	
	public static void sieben() {
		for(int i = 0; i < 200; i++) {
			if(i % 7 == 0) {
				System.out.print(i + " ");
			
			}
		}
	}
		
	public static void fuenfvier() {
		for(int i = 0; i < 200; i++) {
			if (i % 5 != 0 && i % 4 == 0) {
				System.out.print(i + " ");
			}
		}
	}

}
