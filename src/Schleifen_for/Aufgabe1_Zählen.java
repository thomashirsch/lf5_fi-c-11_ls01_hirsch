package Schleifen_for;

import java.util.Scanner;

public class Aufgabe1_Z�hlen {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte geben Sie eine ganze Zahl ein.");
		int eingabe = myScanner.nextInt();
		myScanner.close();
		
		for(int i = 1; i <= eingabe; i++) {
			System.out.print(i + " ");
		}
		
		System.out.print("\n\n");
		
		for(int i = eingabe; i >= 1; i--) {
		System.out.print(i + " ");
		}
	}

}
