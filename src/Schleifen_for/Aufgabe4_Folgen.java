package Schleifen_for;

public class Aufgabe4_Folgen {

	public static void main(String[] args) {

		methode_a();
		System.out.println();
		methode_b();
		System.out.println();
		methode_c();
		System.out.println();
		methode_d();
		System.out.println();
		methode_e();

	}

	public static void methode_a() {
		for (int i = 99; i > 8; i -= 3) {
			System.out.print(i + " ");
		}
	}

	public static void methode_b() {
		int a = 1;
		for (int i = 1; a < 401; a += i += 2) {
			System.out.print(a + " ");
		}
	}

	public static void methode_c() {
		for (int i = 2; i < 103; i += 4) {
			System.out.print(i + " ");
		}
	}

	public static void methode_d() {
		int a = 4;
		for (int i = 4; i < 1025; i += a) {
			a += 8;
			System.out.print(i + " ");
		}
	}

	public static void methode_e() {
		for (int i = 2; i < 32769; i *= 2) {
			System.out.print(i + " ");
		}
	}
}
