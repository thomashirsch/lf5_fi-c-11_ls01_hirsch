package Schleifen_for;

import java.util.Scanner;

public class Aufgabe2_Summe extends Aufgabe1_Z�hlen {

	public static void main(String[] args) {
		System.out.println("Geben Sie bitte einen begrenzenden Wert ein: ");
		Scanner myScanner = new Scanner(System.in);
		int eingabe = myScanner.nextInt();
		zahlenreihe_a(eingabe);
		zahlenreihe_b(eingabe);
		zahlenreihe_c(eingabe);
		myScanner.close();
	}
		
	public static void zahlenreihe_a(int max) {
		int hilfszahl1 = 0;
		int hilfszahl2 = 0;
		
		for(int a = 0; a < max; a++) {
			hilfszahl2 = ++hilfszahl1 + hilfszahl2;
			//System.out.print(hilfszahl1 + " ");
		}
		System.out.println("Die Summe f�r A betr�gt: " + hilfszahl2);
	}
	
	public static void zahlenreihe_b(int max) {
		int hilfszahl1 = 0;
		int hilfszahl2 = 0;
		
		for(int a = 0; a < max; a++) {
			hilfszahl1 = hilfszahl1 + 2;
			hilfszahl2 = hilfszahl1 + hilfszahl2;
		}
		System.out.println("Die Summe f�r B betr�gt: " + hilfszahl2);
	}
	
	public static void zahlenreihe_c(int max) {
		int hilfszahl1 = -1;
		int hilfszahl2 = 0;

		for(int a = 0; a < max; a++) {
			hilfszahl1 = hilfszahl1 + 2;
			hilfszahl2 = hilfszahl1 + hilfszahl2;
		}
		System.out.println("Die Summe f�r C betr�gt: " + hilfszahl2);
	}
	
}